#!/usr/bin/env python

import os
import json

def load_config():
    SHUTTLE_CONFIG = os.environ.get("SHUTTLE_CONFIG", 
        "../config/shuttle.json")
    with open(SHUTTLE_CONFIG) as fp:
        config = json.loads(fp.read())
    
    return config

config = load_config()

FAILED_EMOJIS = [ 
    u':sweat_smile:', u':joy:', u':rolling_on_the_floor_laughing:',
    u':cold_sweat:', ':cry:', u':disappointed_relieved:', u':sob:', u':sweat:',
    u':sleepy:', u':face_with_rolling_eyes:', u':wave:', u':broken_heart:',
    u':guo:', u':watchingyou:'
]